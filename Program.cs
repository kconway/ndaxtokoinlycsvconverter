﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Utility.CommandLine;

namespace NdaxToStdTransactionFormatConverter
{
    class Program
    {
        static List<NdaxLedgerRecord> ndaxLedgerRecords;

        [Argument('i', "input", "The full path to the NDAX ledger csv file (optional, defaults to searching the desktop for a file matching the *ledgers*.csv pattern)")]
        private static string NdaxLedgerFileFullPath { get; set; }
        [Argument('o', "output", "The output file path (optional, defaults to desktop and uses the NDAX file name as a base)")]
        private static string KoinlyOutputFileFullPath { get; set; }

        static void Main(string[] args)
        {
            Arguments.Populate();
            string csvFilePath = String.Empty;

            if (!String.IsNullOrEmpty(NdaxLedgerFileFullPath))
            {
                csvFilePath = NdaxLedgerFileFullPath;
            }
            else
            {
                string searchTerm = "*ledger*.csv";
                string directory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                string[] paths = Directory.GetFiles(directory, searchTerm, SearchOption.TopDirectoryOnly);
                if (paths.Count() != 1)
                {
                    Console.WriteLine("Found " + paths.Count() + " files in " + directory + " containing the search term " + searchTerm + ". Please ensure " +
                        "there is only one file that matches this condition or specify the file by passing the appropriate command line argument (-i /path/to/file).");
                    return;
                }
                else
                {
                    csvFilePath = paths[0];
                }
            }

            ndaxLedgerRecords = new List<NdaxLedgerRecord>();

            using (var reader = new StreamReader(csvFilePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    NdaxLedgerRecord record = new NdaxLedgerRecord();
                    record.DateTime = csv.GetField<DateTime>("DATE");
                    record.TxId = csv.GetField<UInt64>("TX_ID");
                    record.Symbol = csv.GetField<string>("ASSET");
                    record.SetTransType(csv.GetField<string>("TYPE"));
                    record.IsTransfer = (record.TransType == TransactionType.Deposit || record.TransType == TransactionType.Withdrawal) && !record.Symbol.Equals("CAD");
                    record.Amount = csv.GetField<double>("AMOUNT");
                    ndaxLedgerRecords.Add(record);
                }
            }


            List<ulong> processedTxIds = new List<ulong>();
            List<GenericTradeRecord> genericRecords = new List<GenericTradeRecord>();
            foreach (NdaxLedgerRecord record in ndaxLedgerRecords)
            {
                // skip processed txId
                if (processedTxIds.Any(x => x == record.TxId))
                {
                    continue;
                }

                GenericTradeRecord genericRecord = new GenericTradeRecord();

                if ((record.TransType == TransactionType.Trade) && (record.Symbol != "CAD"))
                {
                    NdaxLedgerRecord correspondingLedgerRecord = RetrieveCorrespondingTradeRecord(record);
                    processedTxIds.Add(record.TxId);
                    processedTxIds.Add(correspondingLedgerRecord.TxId);

                    FeeRecord feeRecord = RetrieveFeeRecord(record);
                    NdaxLedgerRecord receiver = (correspondingLedgerRecord.Amount < 0.0) ? record : correspondingLedgerRecord;
                    NdaxLedgerRecord sender = (correspondingLedgerRecord.Amount < 0.0) ? correspondingLedgerRecord : record;

                    genericRecord.DateTime = record.DateTime;
                    genericRecord.ReceivedCurrency = receiver.Symbol;
                    genericRecord.ReceivedQuantity = Math.Abs(receiver.Amount);
                    genericRecord.SentCurrency = sender.Symbol;
                    genericRecord.SentQuantity = Math.Abs(sender.Amount);
                    genericRecord.FeeCurrency = feeRecord.Symbol;
                    genericRecord.FeeAmount = Math.Abs(feeRecord.Fee);
                    genericRecord.TransType = record.TransType;
                    genericRecord.IsTransfer = record.IsTransfer;
                    genericRecords.Add(genericRecord);
                }
                else if (record.TransType == TransactionType.Deposit)
                {
                    genericRecord.DateTime = record.DateTime;
                    genericRecord.ReceivedCurrency = record.Symbol;
                    genericRecord.ReceivedQuantity = record.Amount;
                    genericRecord.SentCurrency = String.Empty;
                    genericRecord.SentQuantity = 0.0;
                    genericRecord.FeeCurrency = String.Empty;
                    genericRecord.FeeAmount = 0.0;
                    genericRecord.TransType = record.TransType;
                    genericRecord.IsTransfer = record.IsTransfer;
                    genericRecords.Add(genericRecord);
                }
                else if (record.TransType == TransactionType.Withdrawal)
                {
                    NdaxLedgerRecord feeRecord = ndaxLedgerRecords.First(x => ((record.DateTime - x.DateTime).Duration() < TimeSpan.FromMilliseconds(10))
                    && x.TransType == TransactionType.Fee);
                    genericRecord.DateTime = record.DateTime;
                    genericRecord.ReceivedCurrency = String.Empty;
                    genericRecord.ReceivedQuantity = 0.0;
                    genericRecord.SentCurrency = record.Symbol;
                    genericRecord.SentQuantity = Math.Abs(record.Amount);
                    genericRecord.FeeCurrency = feeRecord != null ? feeRecord.Symbol : "";
                    genericRecord.FeeAmount = feeRecord != null ? Math.Abs(feeRecord.Amount) : 0.0;
                    genericRecord.TransType = record.TransType;
                    genericRecord.IsTransfer = record.IsTransfer;
                    genericRecords.Add(genericRecord);

                }
            }

            // Write out to file
            string outCsvFilePath = String.Empty;
            if (!String.IsNullOrEmpty(KoinlyOutputFileFullPath))
            {
                outCsvFilePath = KoinlyOutputFileFullPath;
            }
            else
            {
                string delimiterString = "ledgers";
                string inCsvFileName = Path.GetFileNameWithoutExtension(csvFilePath);
                string outCsvFileName = "\\outfile_koinly"
                    + inCsvFileName.Substring(inCsvFileName.IndexOf(delimiterString) + delimiterString.Length)
                    + ".csv";
                outCsvFilePath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + outCsvFileName;
            }

            using (var csvout = new StreamWriter(outCsvFilePath))
            {
                csvout.WriteLine("Date,Received Quantity,Received Currency,Sent Quantity,Sent Currency,Fee Amount,Fee Currency,Tag");
                foreach (GenericTradeRecord record in genericRecords)
                {
                    csvout.WriteLine(record.DateTime.ToUniversalTime().ToString("u") + "," +
                    record.ReceivedQuantity.ToString("0.000000000") + "," +
                    record.ReceivedCurrency + "," +
                    record.SentQuantity.ToString("0.000000000") + "," +
                    record.SentCurrency + "," +
                    record.FeeAmount.ToString("0.000000000") + "," +
                    record.FeeCurrency + ",");
                }
            }

            Console.WriteLine("Completed successfully. Output file is: " + outCsvFilePath);
        }

        static private NdaxLedgerRecord RetrieveCorrespondingTradeRecord(NdaxLedgerRecord record)
        {
            NdaxLedgerRecord correspondingLedgerRecord = new NdaxLedgerRecord();

            // First, see what trade records around the same time can be found
            List<NdaxLedgerRecord> potentialCorrespondingRecords = ndaxLedgerRecords.Where(x => ((record.DateTime - x.DateTime).Duration() < TimeSpan.FromMilliseconds(10))
            && (x.TransType == TransactionType.Trade)
            && (x.TxId != record.TxId)).ToList();

            // if only one, return it
             if (potentialCorrespondingRecords.Count == 1)
            {
                correspondingLedgerRecord = potentialCorrespondingRecords.ElementAt(0);
            }
            // if multiple records exist, work out which is the corresponding record
            else if (potentialCorrespondingRecords.Count > 1)
            {
                // Find corresponding record from same and other symbol records                       
                List<NdaxLedgerRecord> sameSymbolRecords = ndaxLedgerRecords.Where(x => ((record.DateTime - x.DateTime).Duration() < TimeSpan.FromMilliseconds(10))
                && x.TransType == TransactionType.Trade
                && x.Symbol == record.Symbol).ToList();

                List<NdaxLedgerRecord> otherSymbolRecords = ndaxLedgerRecords.Where(x => ((record.DateTime - x.DateTime).Duration() < TimeSpan.FromMilliseconds(10))
                && x.TransType == TransactionType.Trade
                && x.Symbol != record.Symbol).ToList();

                if (sameSymbolRecords.Count == otherSymbolRecords.Count)
                {
                    sameSymbolRecords = sameSymbolRecords.OrderBy(x => Math.Abs(x.Amount)).ToList();
                    otherSymbolRecords = otherSymbolRecords.OrderBy(x => Math.Abs(x.Amount)).ToList();

                    for (int i = 0; i < otherSymbolRecords.Count; i++)
                    {
                        if (record.Amount == sameSymbolRecords.ElementAt(i).Amount)
                        {
                            correspondingLedgerRecord = otherSymbolRecords.ElementAt(i);
                            break;
                        }
                    }
                }
                else
                {
                    throw new InvalidOperationException("Mismatching number of same and other symbol records");
                }
            }
            // if none, assume no fee
            else
            {
                throw new InvalidOperationException("Couldn't find any matching corresponding records");
            }

            return correspondingLedgerRecord;
        }

        static private FeeRecord RetrieveFeeRecord(NdaxLedgerRecord record)
        {
            FeeRecord feeRecord = new FeeRecord();

            // First, see what fee records around the same time can be found
            List<NdaxLedgerRecord> potentialFeeRecords = ndaxLedgerRecords.Where(x => ((record.DateTime - x.DateTime).Duration() < TimeSpan.FromMilliseconds(10))
            && x.TransType == TransactionType.Fee).ToList();

            // if only one, return it
            if (potentialFeeRecords.Count == 1)
            {
                feeRecord.Symbol = potentialFeeRecords.ElementAt(0).Symbol;
                feeRecord.Fee = potentialFeeRecords.ElementAt(0).Amount;
            }
            // if multiple fees exist, work out which is the corresponding record
            else if (potentialFeeRecords.Count > 1)
            {
                // Find fee records from trade data                       
                List<NdaxLedgerRecord> potentialLedgerTradeRecords = ndaxLedgerRecords.Where(x => ((record.DateTime - x.DateTime).Duration() < TimeSpan.FromMilliseconds(10))
                && x.TransType == TransactionType.Trade
                && x.Symbol == record.Symbol).ToList();

                if (potentialFeeRecords.Count == potentialLedgerTradeRecords.Count)
                {
                    potentialFeeRecords = potentialFeeRecords.OrderByDescending(x => x.Amount).ToList(); // fees are negative
                    potentialLedgerTradeRecords = potentialLedgerTradeRecords.OrderBy(x => x.Amount).ToList();

                    for (int i = 0; i < potentialFeeRecords.Count; i++)
                    {
                        if (record.Amount == potentialLedgerTradeRecords.ElementAt(i).Amount)
                        {
                            feeRecord.Symbol = potentialFeeRecords.ElementAt(i).Symbol;
                            feeRecord.Fee = potentialFeeRecords.ElementAt(i).Amount;
                            break;
                        }
                    }
                }
                else
                {
                    throw new InvalidOperationException("Mismatching number of fee and trade records");
                }
            }
            // if none, assume no fee
            else
            {
                feeRecord.Symbol = "";
                feeRecord.Fee = 0.0;
            }

            return feeRecord;
        }
    }
}
