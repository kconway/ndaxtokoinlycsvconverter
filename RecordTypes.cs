﻿using System;

namespace NdaxToStdTransactionFormatConverter
{
    public enum TransactionType
    {
        Deposit,
        Withdrawal,
        Trade,
        Fee,
        Transfer,
        Unknown
    }

    class NdaxLedgerRecord
    {
        public DateTime DateTime;
        public ulong TxId;
        private TransactionType _transType;
        public TransactionType TransType
        {
            get => _transType;
        }
        public string Symbol;
        public double Amount;
        public bool IsTransfer;

        public void SetTransType(string text)
        {
            TransactionType transactionType = TransactionType.Unknown;
            if (text.Contains("FEE"))
            {
                transactionType = TransactionType.Fee;
            }
            else if (text.Contains("DEPOSIT"))
            {
                transactionType = TransactionType.Deposit;
            }
            else if (text.Contains("WITHDRAW"))
            {
                transactionType = TransactionType.Withdrawal;
            }
            else if (text.Contains("TRADE"))
            {
                transactionType = TransactionType.Trade;
            }

            _transType = transactionType;
        }

    }

    class GenericTradeRecord
    {
        public DateTime DateTime;
        public double ReceivedQuantity;
        public string ReceivedCurrency;
        public double SentQuantity;
        public string SentCurrency;
        public double FeeAmount;
        public string FeeCurrency;
        public TransactionType TransType;
        public bool IsTransfer;
    }

    class FeeRecord
    {
        public string Symbol;
        public double Fee;
    }

}
