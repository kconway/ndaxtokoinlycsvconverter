<h1 align="center">NDAX To Koinly Converter</h1>

<h4 align="center">NDAX To Koinly Converter is a simple .Net console program written in C# that takes in 
a csv ledger file produced from NDAX and converts it into a format that can be uploaded to Koinly.</h4>

## Instructions

1. Download a ledger csv file from NDAX.
2. Run the NdaxToKoinlyCsvConverter executable with optional input (-i) and output (-o) file path parameters. 
If no input path is provided, the tool will search the desktop for a csv file matching the *ledger*.csv pattern 
and will use that as the input file. If no output path is provided, the file will be saved on the desktop.
